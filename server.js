var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");
var app = express();
var apiRoutes = express.Router();
app.use(bodyParser.json());
const PersonDao = require("./dao/persondao.js");

var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "sabines",
  password: "YG5Z6XDl",
  database: "sabines",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

app.get("/person", (req, res) => {
  personDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.get("/person/:personId", (req, res) => {
  personDao.getOne(req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.post("/person", (req, res) => {
  personDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.put("/person/:personId", (req, res) => {
  personDao.updateOne(req.body, req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.delete("/person/:personId", (req, res) => {
  personDao.deleteOne(req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

var server = app.listen(8080);
